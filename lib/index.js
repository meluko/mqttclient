'use strict';

const MQTTHandler = require('./MQTTHandler');
const MQTTClient = require('./MQTTClient');
const database = require('./MQTTSubscriptionDatabase');


module.exports = async function (params) {
  const clientid = `${params.clientName}_${Math.random().toString(16).substr(2, 8)}`
  const options = {
    ...params,
    clientid
  };

  const mqttclient = MQTTClient(options);
  const mqtthandler = MQTTHandler(mqttclient, database);

  return mqtthandler
};
