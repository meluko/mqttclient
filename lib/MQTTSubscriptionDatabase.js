'use strict';

const match = require('mqtt-match');

const subscriptions = {};

const addSubscription = function (topic, callback) {
  if (!subscriptions[topic]) {
    subscriptions[topic] = []
  }
  subscriptions[topic].push(callback);
};

const subscriptionsForTopic = function (topic) {
  return Object
    .entries(subscriptions)
    .filter(([subscriptionTopic]) => match(subscriptionTopic, topic))
    .reduce((prev, [, callbacks]) => ([...prev, ...callbacks]), []);
};

const paramsForTopic = function (topic) {
  const matchingPattern = Object
    .keys(subscriptions)
    .find(pattern => match(pattern, topic))

  if (!matchingPattern) {
    return []
  }

  const patternComponents = matchingPattern.split('/');

  let hashFound = false;
  const components = topic.split('/')
    .filter((component, index) => {
      if (hashFound) return true;
      if (patternComponents[index] === '+') {
        return true
      }
      if (patternComponents[index] === '#') {
        hashFound = true;
        return true;
      }
    });
  return components
};

const clearSubscriptions = () => {
  for (let subscription in subscriptions) {
    delete subscriptions[subscription];
  }
}

const getSubscriptions = () => {
  return subscriptions;
}

const removeSubscription = function (topic, callback) {
  subscriptions[topic] = subscriptions[topic]
      .filter(it => it !== callback)
}

module.exports = {
  subscriptionsForTopic,
  addSubscription,
  paramsForTopic,
  getSubscriptions,
  clearSubscriptions,
  removeSubscription
};


