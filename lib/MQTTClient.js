'use strict';

const mqtt = require('mqtt');

module.exports = options => eventHandlers => {
  const {onConnect, onDisconnect, onMessage, onError} = eventHandlers;
  const client = mqtt.connect(options);

  client.on('message', onMessage);
  client.on('disconnect', () => onDisconnect());
  client.on('connect', onConnect);
  client.on('error', onError);

  return client;
};
