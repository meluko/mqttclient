'use strict';

const {EventEmitter} = require('events');
const util = require('util');
const {
  normalizeIncomingMessage,
  normalizeOutgingMessage
} = require('./normalizers');

module.exports = function (client, mqttSubscriptions) {
  const mqttHandler = new EventEmitter();

  const onConnect = () => mqttHandler.emit('connect');
  const onDisconnect = () => mqttHandler.emit('disconnect');
  const onError = error => mqttHandler.emit('error', error);
  const onMessage = function (topic, message) {
    message = normalizeIncomingMessage(message);
    const topicParams = mqttSubscriptions.paramsForTopic(topic);

    mqttSubscriptions.subscriptionsForTopic(topic).forEach(callback => {
      callback(topic, message, ...topicParams)
    });
  };


  mqttHandler.subscribe = (topic, callback) => {
    client.subscribe(topic, () => {});
    mqttSubscriptions.addSubscription(topic, callback)
  };


  mqttHandler.unsubscribe = (topic, callback) => {
    client.unsubscribe(topic, () => {});
    mqttSubscriptions.removeSubscription(topic, callback)
  };

  mqttHandler.publish = (topic, payload) => {
    payload = normalizeOutgingMessage(payload);
    return client.publish(topic, payload);
  };

  client = client({onConnect, onMessage, onError, onDisconnect});
  client.publish = util.promisify(client.publish);

  mqttHandler.end = (force = true) => {
    client.end(force);
  };

  return mqttHandler;
};



