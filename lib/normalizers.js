'use strict';

const normalizeIncomingMessage = function(message) {
  message = message.toString();
  try {
    return JSON.parse(message);
  } catch (error) {
    return message
  }
};

const normalizeOutgingMessage = function(message) {
  if (typeof message !== "string") {
    return JSON.stringify(message);
  }
  return message;
};

module.exports = {
  normalizeIncomingMessage,
  normalizeOutgingMessage
};
