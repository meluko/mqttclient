const {expect} = require('chai')
const MQTTSubscriptionDatabase = require('../lib/MQTTSubscriptionDatabase');


describe('MQTTSubscriptionDatabase', () => {

    beforeEach(() =>{
        MQTTSubscriptionDatabase.clearSubscriptions();
    })



    it('Should add subscription', () => {
        const call = () => {};
        const topic = 'foo/bar/baz';
        MQTTSubscriptionDatabase.addSubscription(topic, call);
        const subscriptions = MQTTSubscriptionDatabase.getSubscriptions();

        expect(subscriptions).to.be.deep.equal({[topic]: [call]});
    });

    it('Should clear subscriptions', () => {
        const call = () => {};
        const topic = 'foo/bar/baz';
        MQTTSubscriptionDatabase.addSubscription(topic, call);
        MQTTSubscriptionDatabase.clearSubscriptions();
        const subscriptions = MQTTSubscriptionDatabase.getSubscriptions();

        expect(subscriptions).to.be.deep.equal({});
    });

    it('Should add more than one subscription', () => {
        const call1 = () => {};
        const call2 = () => {};
        const topic = 'foo/bar/baz';
        MQTTSubscriptionDatabase.addSubscription(topic, call1);
        MQTTSubscriptionDatabase.addSubscription(topic, call2);
        const subscriptions = MQTTSubscriptionDatabase.getSubscriptions();

        expect(subscriptions).to.be.deep.equal({[topic]: [call1, call2]});
    });

    it('should remove specific subscription', () => {
        const call1 = () => {};
        const call2 = () => {};
        const call3 = () => {};
        const topic = 'foo/bar/baz';
        MQTTSubscriptionDatabase.addSubscription(topic, call1);
        MQTTSubscriptionDatabase.addSubscription(topic, call2);
        MQTTSubscriptionDatabase.addSubscription(topic, call3);
        MQTTSubscriptionDatabase.removeSubscription(topic, call2);
        const subscriptions = MQTTSubscriptionDatabase.getSubscriptions();

        expect(subscriptions).to.be.deep.equal({[topic]: [call1, call3]});
    });



});

